import { Controller, Get, Param, Res, Render, HttpException, HttpStatus } from '@nestjs/common';
import 'reflect-metadata';
import { getManager } from 'typeorm';
import { Log } from '../entity/log.entity';
import { Run } from '../entity/run.entity';
import { User } from '../entity/user.entity';
import { SubSystem } from '../entity/sub_system.entity';
import { log } from 'util';
import { Response } from 'express';
import { catchError } from 'rxjs/operators';

@Controller('csv')
export class CsvController {
    @Get()
    @Render('csv')
    root() {
        return { message: 'Hello world!' };
    }

    @Get('colDelimiter=:colDelimiter&tableName=:tableName&columns=:columns')
    make_csv(@Param() params, @Res() res: Response) {
        let repository;
        let table = params.tableName;
        if (table === 'log' || table === 'Log') {
            repository = getManager().getRepository(Log);
        } else if (table === 'Run' || table === 'run') {
            repository = getManager().getRepository(Run);
        } else if (table === 'User' || table === 'user') {
            repository = getManager().getRepository(User);
        } else if (table === 'SubSystem' || table === 'subSystem') {
            repository = getManager().getRepository(SubSystem);
        } else {
            throw new HttpException(`Table '${table}' does not exist.`, 404);
        }
        const colDelimiter = params.colDelimiter;
        let columns = params.columns;
        if (columns === 'got_no_columns') {
            throw new HttpException(`Specify the columns.`, 404);
        }
        columns = params.columns.split(',');
        let data1 = [columns];
        let str = [];
        let result = repository.find({ select: columns }).catch(
            e => {
                console.log('e');
                res.status(HttpStatus.NOT_FOUND).json({ statusCode: 404, message: 'No such columns in the table.' });
            },
        );
        result.then(
            responses => {
                for (let response of responses) {
                    str = [];
                    for (let value of (Object as any).values(response)) {
                        str.push(value.toString());
                    }
                    data1.push(str);
                }
                const fastcsv = require('fast-csv');
                const fs = require('fs');
                const ws = fs.createWriteStream('out.csv');
                fastcsv
                    .write(data1, {
                        headers: true,
                        delimiter: colDelimiter,
                    })
                    .pipe(ws);

            },
        );
        res.download('out.csv');
    }

    /*
    @Get('templateName=:templateName')
    csv_template(@Param() params, @Res() res: Response): string {
        let repository = getManager().getRepository(Run);
        let result = repository.find({ relations: ['logs'] });
        result.then(
            responses => {

            },
        );
        // res.download('out.csv');
        return 'CSV is ready to upload!';
    }
     */
}
