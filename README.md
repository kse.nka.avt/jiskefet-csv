## Description
This bookkeeping system is a system for A Large Ion Collider Experiment (ALICE) to keep track of what is happening to the data produced by the detectors.
The electric signals produced by the various detectors which together are the ALICE detector are being reconstructed, calibrated, compressed and used in numerous but specific ways. It is important to register
how this is done to make a reproduction of data possible and thereby a validation of the information produced. The project is also known as the Jiskefet project.
This is the back-end API for the Jiskefet project.

## Installing Node JS
1. Installing [nodejs](https://nodejs.org/en/).

## Installing Express and Fast-csv
2. Installing express

- npm init
 
- npm install express

3. Installing fast-csv

- npm install fast-csv

## Change orm config
On that:

    {
    "type": "mysql",
    "host": "178.140.17.15",
    "port": 1433,
    "username": "simonfox",
    "password": "89853646639",
    "database": "simonfox_edu",
    "synchronize": true,
    "logging": true,
    "keepConnectionAlive": true,
    "entities": [
    "src/entity/*.ts"
    ],
    "migrations": [
    "src/migration/**/*.ts"
    ],
    "subscribers": [
    "src/subscriber/**/*.ts"
    ],
    "cli": {
    "entitiesDir": "src/entity",
    "migrationsDir": "src/migration",
    "subscribersDir": "src/subscriber"
    }
    }
    
# Проект: «Jiskefet»

Проект является частью системы Alice, используемой БАК для анализа и фильтрации информации.
Основная суть проекта заключается в том, чтобы в удобном для пользователя формате (в данном случае - csv), предоставлять выборочную информацию из БД.

http://jiskefet-api.std-827.ist.mospolytech.ru/csv/

## Участники

| Учебная группа | Имя пользователя | ФИО                      |
|----------------|------------------|--------------------------|
| 181-331        | @kse.nka.avt(lab)| Автономова К.А.          |
|                | @KsenkaA (gitHub)|                          |
| 181-331        | @Mistone (gitlab)| Кутузов В.А.             |
| 181-331        | @SemVolk (gitHub)| Волкодав С.А.            |
| 181-331        | @Cnfhn (lab)     | Егерев И.А.              |

## Личный вклад участников

### Ksenia (объем работы 130 часов)

    1. Switching to net js and TypefORM
    2. The final product
    3. Error correction
    4. The study of frameworks and libraries
    5. Bringing repo to the form recommended for the project
    6. Participation in video conferences with foreign partners 
    7. Embed code in the main project repository
    8. Study of the structure and content of the repo project
    9. System testing deployment project based on ansible 
    10. Working with databases 
    11. Discussion with foreign participants of the found errors and features 
    12. Planned refactoring
    

### Simon (объем работы 110 часов)

    1. Server deployment
    2. Documentation
    3. The development of frameworks and libraries
    4. Study of the structure and content of the repo project
    5. Participation in video conferences with foreign partners 
    6. System testing deployment project based on ansible 
    7. Discussion with foreign participants of the found errors and features
    8. Early version development
    9. Planned refactoring

### Vyacheslav (объем работы 110 часов)

    1. CSV upload 
    2. The development of frameworks and libraries
    3. Participation in video conferences with foreign partners 
    4. Study of the structure and content of the repo project
    5. System testing deployment project based on ansible 
    6. Working with databases 
    7. Discussion with foreign participants of the found errors and especially 
    8. Early version development
    9. Planned refactoring
    10. Front-end

### Ivan (объем работы 130 часов)

    1. Early version development
    2. The development of frameworks and libraries
    3. Study of the structure and content of the repo project
    4. System testing deployment project based on ansible 
    5. Planned refactoring
    6. Testing
    7. Swagger
    8. Documentation
